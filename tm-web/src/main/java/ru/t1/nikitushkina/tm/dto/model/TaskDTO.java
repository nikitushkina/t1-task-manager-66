package ru.t1.nikitushkina.tm.dto.model;

import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;
import ru.t1.nikitushkina.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskDTO {

    private static final long serialVersionUId = 1;

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(length = 150)
    private String name = "";

    @NotNull
    @Column(length = 250)
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    @Column
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    public TaskDTO(@NotNull final String name) {
        this.name = name;
    }

    public TaskDTO(
            @NotNull final String name,
            @NotNull final String description
    ) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
