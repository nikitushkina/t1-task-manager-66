package ru.t1.nikitushkina.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.nikitushkina.tm.model.Session;
import ru.t1.nikitushkina.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    long countByUser(@NotNull final User user);

    void deleteByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @Query("select case when count(s)> 0 then true else false end from Session s where user.id = :userId and id = :id")
    boolean existByUserIdAndId(@Param("userId") String userId, @Param("id") String id);

    @NotNull
    List<Session> findByUser(@NotNull final User user);

    @NotNull
    List<Session> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Session> findByUserAndId(@NotNull final User user, @NotNull final String id);

}
