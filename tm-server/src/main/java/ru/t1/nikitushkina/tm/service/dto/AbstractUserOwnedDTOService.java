package ru.t1.nikitushkina.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nikitushkina.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.nikitushkina.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.nikitushkina.tm.exception.user.UserIdEmptyException;
import ru.t1.nikitushkina.tm.repository.dto.AbstractUserOwnedDTORepository;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTOService<M> implements IUserOwnedDTOService<M> {

    @Nullable
    protected abstract AbstractUserOwnedDTORepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUserId(userId);
        return getRepository().save(model);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (findOneById(model.getUserId(), model.getId()) == null) return;
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (findOneById(model.getUserId(), model.getId()) == null) return;
        model.setUserId(userId);
        getRepository().save(model);
    }

}
