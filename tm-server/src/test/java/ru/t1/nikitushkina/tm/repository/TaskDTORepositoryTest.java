package ru.t1.nikitushkina.tm.repository;

import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.marker.UnitCategory;
import ru.t1.nikitushkina.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
public final class TaskDTORepositoryTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private static ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @BeforeClass
    public static void setUp() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        PropertyService propertyService = new PropertyService();
        ConnectionService connectionService = new ConnectionService(propertyService);

        @NotNull final UserDTO user = userService.create(USER_TEST_LOGIN, USER_TEST_PASSWORD);
        userId = user.getId();
        projectService.add(userId, USER_PROJECT1);
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDTO user = userService.findByLogin(USER_TEST_LOGIN);
        projectService.remove(userId, USER_PROJECT1);
        if (user != null) userService.remove(user);
        connectionService.close();
    }

    @Test
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(userId, USER_TASK3));
            entityManager.getTransaction().commit();
            @Nullable final TaskDTO task = repository.findOneById(userId, USER_TASK3.getId());
            Assert.assertNotNull(task);
            Assert.assertEquals(USER_TASK3.getId(), task.getId());
            Assert.assertEquals(userId, task.getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    public void clean() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
            Assert.assertEquals(0, repository.getSize(userId));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(userId, USER_TASK3.getName());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(userId, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void createByUserIdWithDescription() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final TaskDTO task = repository.create(userId, USER_TASK3.getName(), USER_TASK3.getDescription());
            Assert.assertEquals(USER_TASK3.getName(), task.getName());
            Assert.assertEquals(USER_TASK3.getDescription(), task.getDescription());
            Assert.assertEquals(userId, task.getUserId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(userId, NON_EXISTING_TASK_ID));
        Assert.assertTrue(repository.existsById(userId, USER_TASK1.getId()));
        entityManager.close();
    }

    @Test
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(Collections.emptyList(), repository.findAll(""));
        final List<TaskDTO> tasks = repository.findAll(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void findAllComparatorByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<TaskDTO> tasks = repository.findAll(userId, comparator);
        Assert.assertNotNull(tasks);
        tasks.forEach(task -> Assert.assertEquals(userId, task.getUserId()));
        entityManager.close();
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(userId, NON_EXISTING_TASK_ID));
        @Nullable final TaskDTO task = repository.findOneById(userId, USER_TASK1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(USER_TASK1.getId(), task.getId());
        entityManager.close();
    }

    @Test
    public void getSizeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        Assert.assertEquals(2, repository.getSize(userId));
        entityManager.close();
    }

    @Before
    public void initTest() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, USER_TASK1);
            repository.add(userId, USER_TASK2);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(userId, USER_TASK2);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findOneById(USER_TASK2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void update() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            USER_TASK1.setName(USER_TASK3.getName());
            repository.update(USER_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER_TASK3.getName(), repository.findOneById(userId, USER_TASK1.getId()).getName());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
*/
}
