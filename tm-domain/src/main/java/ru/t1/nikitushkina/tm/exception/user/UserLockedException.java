package ru.t1.nikitushkina.tm.exception.user;

public final class UserLockedException extends AbstractUserException {

    public UserLockedException() {
        super("Error! User is locked.");
    }

}
