package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.ManualSort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ManualSort manualSort;

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}
