package ru.t1.nikitushkina.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.enumerated.ManualSort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private ManualSort manualSort;

    public TaskListRequest(@Nullable String token) {
        super(token);
    }

}
